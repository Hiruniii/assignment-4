/*Write a C program to convert temperature given in celsius to Fahrenheit. You should round
the output value into 2 decimal points*/
#include <stdio.h>
int main (){
	float celsius,fahrenheit;
	printf("Enter the temperature in Celsius : ");
	scanf("%f",&celsius);
	
	fahrenheit=(celsius*9/5)+32;
	
	printf("Temperature in Fahrenheit : %.2f",fahrenheit);
	
	return 0;
}
