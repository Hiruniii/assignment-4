/*Write a program to find the volume of the cone where the 
user inputs the height and radius of the cone*/
#include <stdio.h>
int main (){
	float pi=3.14,radius,height,volume;
    	printf("Input the height : ");
    	 scanf("%f", &height);
	     printf("Input the radius : ");
     	scanf("%f",&radius);
     	
     volume=(pi*(radius*radius)*height/3); //calculate the volume
	 printf("Volume of the cone : %.2f",volume);
	 
	 return 0;	
	
	
}
