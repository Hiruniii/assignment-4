/*Write a C program to calculate the sum and average of any given 3 
integers and print the output*/
#include <stdio.h>
int main (){
	float num1,num2,num3, sum, average;
	
	printf("Enter three integers : ");
	scanf("%f" "%f" "%f", &num1,&num2,&num3);
       sum=num1+num2+num3;    //calculate sum
	   average=sum/3;        //calculate average
	
     	printf("The sum of integers : %.2f\n",sum);
       printf("The average of integers : %.2f\n",average);
	
	return 0;
	
}
