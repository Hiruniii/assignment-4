//Write a C program to demonstrate given bitwise operators given using the same program.
#include <stdio.h>
int main (){
 	int A=10, B=15, C=0;
	
	C = A & B;
	printf("The value  : %d\n",C);
	
	C = A ^ B;
	printf("The value : %d\n",C);
	
	C = ~A;
	printf("The value  : %d\n",C);
	
	C = A << 3;
	printf("The value   : %d\n",C);
	
	C = B >>3 ;
	printf("The value : %d\n",C);
	
	return 0;
}
