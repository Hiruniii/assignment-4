//Write a C program to swap two numbers without using a temporary third variable.
#include <stdio.h>
int main (){
	int a=10 ,b = 30;
	printf("Two numbers : a=%d , b=%d\n",a,b);
	a=a+b;
	b=a-b;
	a=a-b;
	
	printf("After swap numbers : a=%d , b=%d",a,b);
	
	return 0;
}
